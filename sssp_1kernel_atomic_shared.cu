#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "utils.cuh" 
#include "processFiles.cuh"
#include "gputimer.h"
#include "ssspVerify.cuh"

__global__ void SSSP_kernel(Node* d_node, int* d_edges, int *d_weights,
                            bool* d_mask, int* d_cost,
                            unsigned long long no_of_nodes, bool *d_stop) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    __shared__ bool stop;
    
    // Initilization of shared variables is undefined in CUDA - without the below 3 lines of code - the code is stuck in an infinite loop!
    // Changing bool stop to int stop and not using the below code - code still stuck in an infinite loop
    if(threadIdx.x == 0)
    	stop = true;
    __syncthreads();

    if (tid < no_of_nodes && d_mask[tid]) {
        d_mask[tid] = false;

        for (int i = d_node[tid].start; i < (d_node[tid].start + d_node[tid].num); i++) {
            int id = d_edges[i];
            int temp = d_cost[tid] + d_weights[i];
            if (d_cost[id] > temp && temp >= 0) {
                atomicMin(&d_cost[id], temp);
                d_mask[id] = true;
                stop = false;
            }
        }
    }
    __syncthreads();
    if(threadIdx.x == 0 && !stop)
    	*d_stop = false;
}

int main(int argc, char** argv) {

	UINT64 no_of_nodes, no_of_edges;
	GpuTimer timer, h_timer;
	Node *h_node;
	int *h_edges, *h_cost, *h_weights, *h_costU;
	bool *h_mask;

	// All elements passed by reference
	processFile(h_node, h_edges, h_weights, no_of_nodes, no_of_edges);

	h_cost = (int *)malloc(sizeof(int) * no_of_nodes);
	h_costU = (int *)malloc(sizeof(int) * no_of_nodes);
	h_mask = (bool *)malloc(sizeof(bool) * no_of_nodes);

	for(int i = 0; i < no_of_nodes; i++) {
		h_mask[i] = true;
		h_cost[i] = INT_MAX;
		h_costU[i] = INT_MAX;
	}
 
	int source = 0; 
	h_cost[source] = 0;

	Node* d_node; 
	cudaMalloc((void**)&d_node, sizeof(Node) * no_of_nodes); 
	cudaMemcpy(d_node, h_node, sizeof(Node) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	int* d_edges; 
	cudaMalloc((void**)&d_edges, sizeof(int) * no_of_edges); 
	cudaMemcpy(d_edges, h_edges, sizeof(int) * no_of_edges, cudaMemcpyHostToDevice);

	int* d_weights; 
	cudaMalloc((void**)&d_weights, sizeof(int) * no_of_edges); 
	cudaMemcpy(d_weights, h_weights, sizeof(int) * no_of_edges, cudaMemcpyHostToDevice);
 
	bool* d_mask; 
	cudaMalloc((void**)&d_mask, sizeof(bool) * no_of_nodes); 
	cudaMemcpy(d_mask, h_mask, sizeof(bool) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	int* d_cost; 
	cudaMalloc((void**)&d_cost, sizeof(int) * no_of_nodes); 
	cudaMemcpy(d_cost, h_cost, sizeof(int) * no_of_nodes, cudaMemcpyHostToDevice);
 
	bool stop; 
	bool* d_stop; 
	cudaMalloc((void**)&d_stop, sizeof(bool)); 

	timer.Start();
	do { 
		stop = true; 
		cudaMemcpy(d_stop, &stop, sizeof(bool), cudaMemcpyHostToDevice); 
		// SSSP_kernel_1<<<((no_of_nodes + ONED_BLOCK_SIZE - 1) / ONED_BLOCK_SIZE), ONED_BLOCK_SIZE>>>(d_node, d_edges, d_weights, d_mask, d_cost, d_costU, no_of_nodes);
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaDeviceSynchronize());
		SSSP_kernel<<<((no_of_nodes + ONED_BLOCK_SIZE - 1) / ONED_BLOCK_SIZE), ONED_BLOCK_SIZE>>>(d_node, d_edges, d_weights, d_mask, d_cost, no_of_nodes, d_stop);
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaDeviceSynchronize());
		cudaMemcpy(&stop, d_stop, sizeof(bool), cudaMemcpyDeviceToHost); 
	} while(!stop);
	timer.Stop();

	cudaMemcpy(h_cost, d_cost, sizeof(int) * no_of_nodes, cudaMemcpyDeviceToHost);
	sssp_verify(h_node, h_edges, h_weights, h_cost, no_of_nodes, h_timer);
	printf("Time taken on GPU: %g ms on host: %g ms\n", timer.Elapsed(), h_timer.Elapsed());
	// for(int i = 0; i < no_of_nodes; i++)
	// 	printf("%d\n", h_cost[i]);
	

	free(h_node); free(h_cost); free(h_mask); free(h_weights); free(h_edges); free(h_costU);
	cudaFree(d_node); cudaFree(d_edges); cudaFree(d_weights); cudaFree(d_mask); cudaFree(d_cost); cudaFree(d_stop);
	return(0);
} 