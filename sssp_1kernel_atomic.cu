#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "utils.cuh" 
#include "processFiles.cuh"
#include "gputimer.h"
#include "ssspVerify.cuh"

__global__ void SSSP_kernel(Node* d_node, int* d_edges, int *d_weights,
                            bool* d_mask, int* d_cost,
                            unsigned long long no_of_nodes, bool *d_stop) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;

    if (tid < no_of_nodes && d_mask[tid]) {
        d_mask[tid] = false;

        for (int i = d_node[tid].start; i < (d_node[tid].start + d_node[tid].num); i++) {
            int id = d_edges[i];

            // Does not work
            // Reason: Race condition at d_mask[tid]. A true value can be overwritten by false and when calculating d_cost[id], the d_cost[tid] might not fetch the latest value (the value which made d_mask[tid] to be true)
            // and the node is stuck in its previous value.
            // int temp = d_cost[tid] + d_weights[i];
            // if (d_cost[id] > temp && temp >= 0) {
            //     d_cost[id] = temp;
            //     d_mask[id] = true;
            //     *d_stop = false;
            // }

            // atomic version - working - slow
            // int temp = d_cost[tid] + d_weights[i];
            // if (d_cost[id] > temp && temp >= 0) {
            //     int oldCost = atomicMin(&d_cost[id], temp);
            //     if (temp < oldCost) {
            //     	d_mask[id] = true;
            //     	*d_stop = false;
            //     }
            // }

            // atomic version - working - fast
            // How does it work?
            // atomicMin, I am assuming will broadcast the d_cost[id] value to all the threads, even to the ones belonging to other blocks?
            int temp = d_cost[tid] + d_weights[i];
            if (d_cost[id] > temp && temp >= 0) {
                atomicMin(&d_cost[id], temp);
                d_mask[id] = true;
                *d_stop = false;
            }

            // atomic version working
            // if ((d_cost[id] > (d_cost[tid] + d_weights[i])) && (d_cost[tid] + d_weights[i]) >= 0) {
            //     atomicMin(&d_cost[id], (d_cost[tid] + d_weights[i]));
            //     d_mask[id] = true;
            //     *d_stop = false;
            // }

        }
    }
}

int main(int argc, char** argv) {

	UINT64 no_of_nodes, no_of_edges;
	GpuTimer timer, h_timer;
	Node *h_node;
	int *h_edges, *h_cost, *h_weights, *h_costU;
	bool *h_mask;

	// All elements passed by reference
	processFile(h_node, h_edges, h_weights, no_of_nodes, no_of_edges);

	h_cost = (int *)malloc(sizeof(int) * no_of_nodes);
	h_costU = (int *)malloc(sizeof(int) * no_of_nodes);
	h_mask = (bool *)malloc(sizeof(bool) * no_of_nodes);

	for(int i = 0; i < no_of_nodes; i++) {
		h_mask[i] = true;
		h_cost[i] = INT_MAX;
		h_costU[i] = INT_MAX;
	}
 
	int source = 0; 
	h_cost[source] = 0;

	Node* d_node; 
	cudaMalloc((void**)&d_node, sizeof(Node) * no_of_nodes); 
	cucheck_dev(cudaGetLastError());
	cudaMemcpy(d_node, h_node, sizeof(Node) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	int* d_edges; 
	cudaMalloc((void**)&d_edges, sizeof(int) * no_of_edges);
	cucheck_dev(cudaGetLastError());
	cudaMemcpy(d_edges, h_edges, sizeof(int) * no_of_edges, cudaMemcpyHostToDevice);

	int* d_weights; 
	cudaMalloc((void**)&d_weights, sizeof(int) * no_of_edges);
	cucheck_dev(cudaGetLastError());
	cudaMemcpy(d_weights, h_weights, sizeof(int) * no_of_edges, cudaMemcpyHostToDevice);
 
	bool* d_mask; 
	cudaMalloc((void**)&d_mask, sizeof(bool) * no_of_nodes);
	cucheck_dev(cudaGetLastError());
	cudaMemcpy(d_mask, h_mask, sizeof(bool) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	int* d_cost; 
	cudaMalloc((void**)&d_cost, sizeof(int) * no_of_nodes);
	cucheck_dev(cudaGetLastError()); 
	cudaMemcpy(d_cost, h_cost, sizeof(int) * no_of_nodes, cudaMemcpyHostToDevice);
 
	bool stop; 
	bool* d_stop; 
	cudaMalloc((void**)&d_stop, sizeof(bool)); 

	timer.Start();
	do { 
		stop = true; 
		cudaMemcpy(d_stop, &stop, sizeof(bool), cudaMemcpyHostToDevice); 
		// SSSP_kernel_1<<<((no_of_nodes + ONED_BLOCK_SIZE - 1) / ONED_BLOCK_SIZE), ONED_BLOCK_SIZE>>>(d_node, d_edges, d_weights, d_mask, d_cost, d_costU, no_of_nodes);
		cucheck_dev(cudaGetLastError());
		SSSP_kernel<<<((no_of_nodes + ONED_BLOCK_SIZE - 1) / ONED_BLOCK_SIZE), ONED_BLOCK_SIZE>>>(d_node, d_edges, d_weights, d_mask, d_cost, no_of_nodes, d_stop);
		cucheck_dev(cudaGetLastError());
		cudaMemcpy(&stop, d_stop, sizeof(bool), cudaMemcpyDeviceToHost); 
	} while(!stop);
	timer.Stop();

	cudaMemcpy(h_cost, d_cost, sizeof(int) * no_of_nodes, cudaMemcpyDeviceToHost);
	sssp_verify(h_node, h_edges, h_weights, h_cost, no_of_nodes, h_timer);
	printf("Time taken on GPU: %g ms on host: %g ms\n", timer.Elapsed(), h_timer.Elapsed());
	// for(int i = 0; i < no_of_nodes; i++)
	// 	printf("%d\n", h_cost[i]);
	

	free(h_node); free(h_cost); free(h_mask); free(h_weights); free(h_edges); free(h_costU);
	cudaFree(d_node); cudaFree(d_edges); cudaFree(d_weights); cudaFree(d_mask); cudaFree(d_cost); cudaFree(d_stop);
	return(0);
} 