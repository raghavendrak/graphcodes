void processFile(Node *&h_node, int *&h_edges, int *&h_weights, unsigned long long &no_of_nodes, unsigned long long &no_of_edges) {
	FILE *gFile;
	gFile = fopen("/home/raghu/cudaWork/datasets/soc-LiveJournal1.txt", "r");
	if(!gFile) {
		printf("Error reading file\n");
		exit(1);
	}
	while(!feof(gFile)) {
		char dummy[1000];
		fscanf(gFile, "%s", dummy);
		if(!strcmp(dummy, "Nodes:"))
			fscanf(gFile, "%lld", &no_of_nodes);
		if(!strcmp(dummy, "Edges:"))
			fscanf(gFile, "%lld", &no_of_edges);
		if(!strcmp(dummy, "ToNodeId"))
			break;
	}
	printf ("nodes: %llu edges: %llu\n", no_of_nodes, no_of_edges);
	unsigned long long processedEdges = 0;
	int edgesPerNode = 0;
	int fromNode, toNode, currentNodeId = 0, prevNodeId = 0;

	h_node = (Node *)malloc(sizeof(Node) * no_of_nodes);
	h_edges = (int *)malloc(sizeof(int) * no_of_edges);
	h_weights = (int *)malloc(sizeof(int) * no_of_edges);
	
	// Initializing all weights to 1
	for(unsigned long long i = 0; i < no_of_edges; i++)
		h_weights[i] = 1;

	while (processedEdges < no_of_edges) {
		fscanf(gFile, "%d%d", &fromNode, &toNode);
		h_edges[processedEdges++] = toNode;
		edgesPerNode++;
		while(currentNodeId < fromNode) {
			if(h_node[currentNodeId].start == 0 && currentNodeId != 0)
				h_node[currentNodeId].start = processedEdges - 1;
			currentNodeId++;
		}
		if(prevNodeId != fromNode) {
			h_node[prevNodeId].num = edgesPerNode - 1;
			currentNodeId = fromNode;
			assert(currentNodeId == fromNode);
			h_node[currentNodeId].start = processedEdges - 1;
			edgesPerNode = 1;
		}
		prevNodeId = fromNode;
	}

	if(currentNodeId < no_of_nodes) {
		h_node[prevNodeId].num = edgesPerNode;
	}
	while(currentNodeId < no_of_nodes) {
		currentNodeId++;
		if(h_node[currentNodeId].start == 0)
			h_node[currentNodeId].start = processedEdges - 1;
	}
	fclose(gFile);
}