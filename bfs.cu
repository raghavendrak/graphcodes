#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define ONED_BLOCK_SIZE 1024


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

typedef struct { 
	int start; 
	int num; 
} Node; 

__global__ void BFS(Node* d_node, int* d_edges, bool* d_frontier, bool* d_visited, int* d_cost, bool* d_stop, unsigned long long no_of_nodes) { 
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid < no_of_nodes && d_frontier[tid]) { 
		d_frontier[tid] = false; d_visited[tid] = true;
		for(int i = d_node[tid].start; i < (d_node[tid].start + d_node[tid].num); i++) { 
			int id = d_edges[i];
			if(!d_visited[id]) {
				if(d_cost[id] > (d_cost[tid] + 1))
					d_cost[id] = d_cost[tid] + 1; 
				d_frontier[id] = true; 
				*d_stop = false; 
			} 
		} 
	} 
} 
 
int main(int argc, char** argv) {

	unsigned long long no_of_nodes, no_of_edges;

	// Process file
	FILE *gFile;
	gFile = fopen("soc-LiveJournal1.txt", "r");
	if(!gFile) {
		printf("Error reading file\n");
		exit(1);
	}
	while(!feof(gFile)) {
		char dummy[1000];
		fscanf(gFile, "%s", dummy);
		if(!strcmp(dummy, "Nodes:"))
			fscanf(gFile, "%lld", &no_of_nodes);
		if(!strcmp(dummy, "Edges:"))
			fscanf(gFile, "%lld", &no_of_edges);
		if(!strcmp(dummy, "ToNodeId"))
			break;
	}

	Node *h_node;
	int *h_edges, *h_cost;
	bool *h_frontier, *h_visited;
	h_node = (Node *)malloc(sizeof(Node) * no_of_nodes);
	h_edges = (int *)malloc(sizeof(int) * no_of_edges);
	h_cost = (int *)malloc(sizeof(int) * no_of_nodes);
	h_frontier = (bool *)malloc(sizeof(bool) * no_of_nodes);
	h_visited = (bool *)malloc(sizeof(bool) * no_of_nodes);

	unsigned long long processedEdges = 0;
	int edgesPerNode = 0;
	int fromNode, toNode, currentNodeId = 0, prevNodeId = 0;
	for(int i = 0; i < no_of_nodes; i++) {
		h_node[i].start = h_node[i].num = 0;
		h_frontier[i] = h_visited[i] = false;
		h_cost[i] = INT_MAX;;
	}

	while (processedEdges < no_of_edges) {
		fscanf(gFile, "%d%d", &fromNode, &toNode);
		h_edges[processedEdges++] = toNode;
		edgesPerNode++;
		while(currentNodeId < fromNode) {
			if(h_node[currentNodeId].start == 0 && currentNodeId != 0)
				h_node[currentNodeId].start = processedEdges - 1;
			currentNodeId++;
		}
		if(prevNodeId != fromNode) {
			h_node[prevNodeId].num = edgesPerNode - 1;
			currentNodeId = fromNode;
			assert(currentNodeId == fromNode);
			h_node[currentNodeId].start = processedEdges - 1;
			edgesPerNode = 1;
		}
		prevNodeId = fromNode;
	}

	if(currentNodeId < no_of_nodes) {
		h_node[prevNodeId].num = edgesPerNode;
	}
	while(currentNodeId < no_of_nodes) {
		currentNodeId++;
		if(h_node[currentNodeId].start == 0)
			h_node[currentNodeId].start = processedEdges - 1;
	}
 
	int source = 0; 
	h_frontier[source] = true;
	h_cost[source] = 0;
	 
	Node* d_node; 
	cudaMalloc((void**)&d_node, sizeof(Node) * no_of_nodes); 
	cudaMemcpy(d_node, h_node, sizeof(Node) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	int* d_edges; 
	cudaMalloc((void**)&d_edges, sizeof(int) * no_of_edges); 
	cudaMemcpy(d_edges, h_edges, sizeof(int) * no_of_edges, cudaMemcpyHostToDevice); 
 
	bool* d_frontier; 
	cudaMalloc((void**)&d_frontier, sizeof(bool) * no_of_nodes); 
	cudaMemcpy(d_frontier, h_frontier, sizeof(bool) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	bool* d_visited; 
	cudaMalloc((void**)&d_visited, sizeof(bool) * no_of_nodes); 
	cudaMemcpy(d_visited, h_visited, sizeof(bool) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	int* d_cost; 
	cudaMalloc((void**)&d_cost, sizeof(int) * no_of_nodes); 
	cudaMemcpy(d_cost, h_cost, sizeof(int) * no_of_nodes, cudaMemcpyHostToDevice); 
 
	bool stop; 
	bool* d_stop; 
	cudaMalloc((void**)&d_stop, sizeof(bool)); 
 
	do { 
		stop = true; 
		cudaMemcpy(d_stop, &stop, sizeof(bool), cudaMemcpyHostToDevice); 
		BFS<<<((no_of_nodes + ONED_BLOCK_SIZE - 1) / ONED_BLOCK_SIZE), ONED_BLOCK_SIZE>>>(d_node, d_edges, d_frontier, d_visited, d_cost, d_stop, no_of_nodes);
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaDeviceSynchronize());
		cudaMemcpy(&stop, d_stop, sizeof(bool), cudaMemcpyDeviceToHost); 
	} while(!stop); 

	cudaMemcpy(h_cost, d_cost, sizeof(int) * no_of_nodes, cudaMemcpyDeviceToHost);
	// for(int i = 0; i < 100; i++)
	// 	printf("i: %d cost: %d\n", i, h_cost[i]); 
	return(0);
} 