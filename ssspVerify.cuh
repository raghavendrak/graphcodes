#include <cstdio>
#include <vector>
#include <queue>
using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void sssp_verify(Node *node, int *edges, int *weights, int *cost, UINT64 no_of_nodes, GpuTimer &timer) {
	vi dist(no_of_nodes, INT_MAX); dist[0] = 0;
	priority_queue< ii, vector<ii>, greater<ii> > pq; pq.push(ii(0, 0));
	timer.Start();
  	while (!pq.empty()) {                                             
    	ii front = pq.top(); pq.pop();
   		int d = front.first, u = front.second;
    	if (d > dist[u]) 
    		continue;   
    	for (int j = node[u].start; j < (node[u].start + node[u].num); j++) {
      		ii v = ii(edges[j], weights[j]);                       
      		if (dist[u] + v.second < dist[v.first]) {
       			dist[v.first] = dist[u] + v.second;
       			pq.push(ii(dist[v.first], v.first));
       		}
       	}
    }
    timer.Stop();
    for (UINT64 i = 0; i < no_of_nodes; i++) {
    	if (cost[i] != dist[i]) {
    		printf("Error at %llu gpu_computed_cost: %d host_computed_cost: %d\n", i, cost[i], dist[i]);
    		exit(1);
    	}
    }
}